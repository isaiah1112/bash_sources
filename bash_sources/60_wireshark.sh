### Wireshark Additions
### Author: Jesse Almanrode (https://about.me/JesseAlmanrode)
### License: GNU GPLv3 (https://choosealicense.com/licenses/gpl-3.0/)

# Set perms for wireshark
sudo chmod 705 /dev/bpf*;